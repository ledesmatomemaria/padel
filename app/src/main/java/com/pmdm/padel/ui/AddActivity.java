package com.pmdm.padel.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CalendarView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.pmdm.padel.databinding.ActivityAddBinding;
import com.pmdm.padel.model.AddResponse;
import com.pmdm.padel.model.Book;
import com.pmdm.padel.network.ApiTokenRestClient;
import com.pmdm.padel.util.SharedPreferencesManager;

import java.io.IOException;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AddActivity extends AppCompatActivity implements View.OnClickListener, Callback<AddResponse> {
    public static final int OK = 1;
    public static long fechaMilisegundos;

    ProgressDialog progreso;
    SharedPreferencesManager preferences;
    private String[] horas;
    private ActivityAddBinding binding;
    private String fechaSelected;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //activity_main.xml -> ActivityMainBinding
        binding = ActivityAddBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        binding.accept.setOnClickListener(this);
        binding.cancel.setOnClickListener(this);

        long time = System.currentTimeMillis();
        binding.reservaCalendario.setMinDate(time);
        binding.reservaCalendario.setMaxDate(time + 1209600000);

        binding.reservaCalendario.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
                String myDate = year + "/" + month + "/" + dayOfMonth;
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    Date date = df.parse(myDate);
                    long fechaMilis = date.getTime();
                    fechaMilisegundos = fechaMilis;
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });

        horas = new String[12];
        horas[0] = "09:00";
        horas[1] = "10:00";
        horas[2] = "11:00";
        horas[3] = "12:00";
        horas[4] = "13:00";
        horas[5] = "14:00";
        horas[6] = "15:00";
        horas[7] = "16:00";
        horas[8] = "17:00";
        horas[9] = "18:00";
        horas[10] = "19:00";
        horas[11] = "20:00";

        binding.reservaHora.setMinValue(0);
        binding.reservaHora.setMaxValue(horas.length-1);
        binding.reservaHora.setDisplayedValues(horas);
        binding.reservaHora.setValue(0);
        binding.reservaHora.setWrapSelectorWheel(false);

        preferences = new SharedPreferencesManager(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onClick(View v) {

        Book book;

        if (v == binding.accept) {
            hideSoftKeyboard();

            Toast.makeText(this, "Milis: " + fechaMilisegundos, Toast.LENGTH_LONG).show();
            book = new Book("2021-06-20", "13:00");
            connection(book);
            /*if (fechaMilisegundos == 0)
                Toast.makeText(this, "Introduzca una fecha", Toast.LENGTH_SHORT).show();
            else {
                book = new Book("2021-06-20", "13:00");
                connection(book);
            }*/
        } else if (v == binding.cancel) {
            finish();
        }
    }

    private void connection(Book book) {
        progreso = new ProgressDialog(this);
        progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progreso.setMessage("Connecting . . .");
        progreso.setCancelable(false);
        progreso.show();

        //Call<Site> call = ApiRestClient.getInstance().createSite("Bearer " + preferences.getToken(), s);
        Call<AddResponse> call = ApiTokenRestClient.getInstance(preferences.getToken()).createBook(book);
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<AddResponse> call, Response<AddResponse> response) {
        progreso.dismiss();
        if (response.isSuccessful()) {
            AddResponse addResponse = response.body();
            if (addResponse.getSuccess()) {
                Intent i = new Intent();
                Bundle bundle = new Bundle();
                bundle.putInt("id", addResponse.getData().getId());
                bundle.putString("fecha", addResponse.getData().getFecha());
                bundle.putString("hora", addResponse.getData().getHora());
                bundle.putString("createdAt", addResponse.getData().getCreatedAt());
                i.putExtras(bundle);
                setResult(OK, i);
                finish();
                showMessage("Book created ok");
            } else {
                String message = "Error creating the task";
                if (!addResponse.getMessage().isEmpty()) {
                    message += ": " + addResponse.getMessage();
                }
                showMessage(message);

            }
        } else {
            StringBuilder message = new StringBuilder();
            message.append("Download error: ");
            if (response.body() != null)
                message.append("\n" + response.body());
            if (response.errorBody() != null)
                try {
                    message.append("\n" + response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            showMessage(message.toString());
        }
    }

    @Override
    public void onFailure(Call<AddResponse> call, Throwable t) {
        progreso.dismiss();
        if (t != null)
            showMessage("Failure in the communication\n" + t.getMessage());
    }

    private void showMessage(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    public void hideSoftKeyboard() {
        if(getCurrentFocus()!=null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }
}

