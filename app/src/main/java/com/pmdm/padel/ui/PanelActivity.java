package com.pmdm.padel.ui;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.pmdm.padel.MainActivity;
import com.pmdm.padel.R;
import com.pmdm.padel.adapter.ClickListener;
import com.pmdm.padel.adapter.RecyclerTouchListener;
import com.pmdm.padel.adapter.TodoAdapter;
import com.pmdm.padel.databinding.ActivityPanelBinding;
import com.pmdm.padel.model.Book;
import com.pmdm.padel.model.DeleteResponse;
import com.pmdm.padel.model.GetTasksResponse;
import com.pmdm.padel.model.LogoutResponse;
import com.pmdm.padel.network.ApiTokenRestClient;
import com.pmdm.padel.util.SharedPreferencesManager;

import java.io.IOException;
import java.sql.Date;
import java.sql.Time;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PanelActivity extends AppCompatActivity implements View.OnClickListener {

    public static final int ADD_CODE = 100;
    public static final int UPDATE_CODE = 200;
    public static final int OK = 1;

    int positionClicked;
    ProgressDialog progreso;
    //ApiService apiService;
    SharedPreferencesManager preferences;
    private ActivityPanelBinding binding;
    private TodoAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //activity_main.xml -> ActivityMainBinding
        binding = ActivityPanelBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        binding.floatingActionButton.setOnClickListener(this);

        preferences = new SharedPreferencesManager(this);
        //showMessage("panel: " + preferences.getToken());

        //Initialize RecyclerView
        adapter = new TodoAdapter();
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        binding.recyclerView.setAdapter(adapter);

        //manage click
        binding.recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this, binding.recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, final int position) {
                showMessage("Single Click on book with id: " + adapter.getAt(position).getId());
                modify(adapter.getAt(position));
                positionClicked = position;
            }

            @Override
            public void onLongClick(View view, int position) {
                showMessage("Long press on position :" + position);
                confirm(adapter.getAt(position).getId(), adapter.getAt(position).getFecha()/*, adapter.getAt(position).getHora()*/, position);
            }
        }));

        //Destruir la instancia de Retrofit para que se cree una con el nuevo token
        ApiTokenRestClient.deleteInstance();

        downloadBook();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_toolbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.refresh:
                //petición al servidor para descargar de nuevo los sitios
                downloadBook();
                break;

            case R.id.email:
                //send an email
                Intent i = new Intent(this, EmailActivity.class);
                startActivity(i);
                break;

            case R.id.exit:
                //petición al servidor para anular el token (a la ruta /api/logout)
                Call<LogoutResponse> call = ApiTokenRestClient.getInstance(preferences.getToken()).logout();
                call.enqueue(new Callback<LogoutResponse>() {
                    @Override
                    public void onResponse(Call<LogoutResponse> call, Response<LogoutResponse> response) {
                        if (response.isSuccessful()) {
                            LogoutResponse logoutResponse = response.body();
                            if (logoutResponse.getSuccess()) {
                                showMessage("Logout OK");
                            } else
                                showMessage("Error in logout");
                        } else {
                            StringBuilder message = new StringBuilder();
                            message.append("Download error: " + response.code());
                            if (response.body() != null)
                                message.append("\n" + response.body());
                            if (response.errorBody() != null)
                                try {
                                    message.append("\n" + response.errorBody().string());
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            showMessage(message.toString());
                        }
                    }

                    @Override
                    public void onFailure(Call<LogoutResponse> call, Throwable t) {
                        String message = "Failure in the communication\n";
                        if (t != null)
                            message += t.getMessage();
                        showMessage(message);

                    }
                });
                preferences.saveToken(null, null);
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                finish();
                break;
        }
        return true;
    }

    @Override
    public void onClick(View v) {

        if (v == binding.floatingActionButton) {
            Intent i = new Intent(this, AddActivity.class);
            startActivityForResult(i, ADD_CODE);
        }
    }

    private void downloadBook() {
        progreso = new ProgressDialog(this);
        progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progreso.setMessage("Connecting . . .");
        progreso.setCancelable(false);
        progreso.show();

        //Call<ArrayList<Site>> call = ApiRestClient.getInstance().getSites("Bearer " + preferences.getToken());
        Call<GetTasksResponse> call = ApiTokenRestClient.getInstance(preferences.getToken()).getBooks();
        call.enqueue(new Callback<GetTasksResponse>() {
            @Override
            public void onResponse(Call<GetTasksResponse> call, Response<GetTasksResponse> response) {
                progreso.dismiss();
                if (response.isSuccessful()) {
                    GetTasksResponse getTasksResponse = response.body();
                    if (getTasksResponse.getSuccess()) {
                        adapter.setBooks(getTasksResponse.getData());
                        showMessage("Books downloaded ok");
                    } else {
                        showMessage("Error downloading the books: " + getTasksResponse.getMessage());
                    }
                } else {
                    StringBuilder message = new StringBuilder();
                    message.append("Download error: " + response.code());
                    if (response.body() != null)
                        message.append("\n" + response.body());
                    if (response.errorBody() != null)
                        try {
                            message.append("\n" + response.errorBody().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    showMessage(message.toString());
                }
            }

            @Override
            public void onFailure(Call<GetTasksResponse> call, Throwable t) {
                progreso.dismiss();
                String message = "Failure in the communication\n";
                if (t != null)
                    message += t.getMessage();
                showMessage(message);
            }
        });
    }

    private void showMessage(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Book book = new Book();

        if (requestCode == ADD_CODE)
            if (resultCode == OK) {
                book.setId(data.getIntExtra("id", 1));
                //book.setFecha(data.getStringExtra("fecha"));
                //book.setHora(data.get);
                book.setCreatedAt(data.getStringExtra("createdAt"));
                adapter.add(book);
            }

        if (requestCode == UPDATE_CODE)
            if (resultCode == OK) {
                book.setId(data.getIntExtra("id", 1));
                //book.setDescription(data.getStringExtra("description"));
                book.setCreatedAt(data.getStringExtra("createdAt"));
                adapter.modifyAt(book, positionClicked);
            }
    }

    private void modify(Book book) {
        Intent i = new Intent(this, UpdateActivity.class);
        i.putExtra("book", book);
        startActivityForResult(i, UPDATE_CODE);
    }

    private void confirm(final int idTask, String fecha,/*Date fecha, Time hora,*/final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        //builder.setMessage(fecha + " - " + hora + "\nDo you want to delete?")
        builder.setMessage(fecha + " - " + "\nDo you want to delete?")
                .setTitle("Eliminar")
                .setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        connection(position);
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        builder.show();
    }

    private void connection(final int position) {
        //Call<ResponseBody> call = ApiRestClient.getInstance().deleteSite("Bearer " + preferences.getToken(), adapter.getId(position));
        Call<DeleteResponse> call = ApiTokenRestClient.getInstance(preferences.getToken()).deleteBook(adapter.getId(position));
        progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progreso.setMessage("Connecting . . .");
        progreso.setCancelable(false);
        progreso.show();
        call.enqueue(new Callback<DeleteResponse>() {
            @Override
            public void onResponse(Call<DeleteResponse> call, Response<DeleteResponse> response) {
                progreso.dismiss();
                if (response.isSuccessful()) {
                    DeleteResponse deleteResponse = response.body();
                    if (deleteResponse.getSuccess()) {
                        adapter.removeAt(position);
                        showMessage("Book deleted OK");
                    } else
                        showMessage("Error deleting the task");
                } else {
                    StringBuilder message = new StringBuilder();
                    message.append("Error deleting a site: " + response.code());
                    if (response.body() != null)
                        message.append("\n" + response.body());
                    if (response.errorBody() != null)
                        try {
                            message.append("\n" + response.errorBody().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    showMessage(message.toString());
                }
            }

            @Override
            public void onFailure(Call<DeleteResponse> call, Throwable t) {
                progreso.dismiss();
                if (t != null)
                    showMessage("Failure in the communication\n" + t.getMessage());

            }
        });
    }
}