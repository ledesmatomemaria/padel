package com.pmdm.padel.network;

import com.pmdm.padel.model.LogoutResponse;
import com.pmdm.padel.model.GetTasksResponse;
import com.pmdm.padel.model.AddResponse;
import com.pmdm.padel.model.DeleteResponse;
import com.pmdm.padel.model.EmailResponse;
import com.pmdm.padel.model.Book;
import com.pmdm.padel.model.Email;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ApiTokenService {

    @POST("api/logout")
    Call<LogoutResponse> logout(
            //@Header("Authorization") String token
    );

    @GET("api/books")
    Call<GetTasksResponse> getBooks(
            //@Header("Authorization") String token
    );

    @POST("api/books/store")
    Call<AddResponse> createBook(
            //@Header("Authorization") String token,
            @Body Book book);

    @PUT("api/books/{book}")
    Call<AddResponse> updateBook(
            //@Header("Authorization") String token,
            @Body Book book,
            @Path("id") int id);

    @DELETE("api/books/{book}")
    Call<DeleteResponse> deleteBook(
            //@Header("Authorization") String token,
            @Path("id") int id);

    @POST("api/email")
    Call<EmailResponse> sendEmail(@Body Email email);
}

