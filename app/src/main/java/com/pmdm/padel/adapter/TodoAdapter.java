package com.pmdm.padel.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pmdm.padel.databinding.ItemViewBinding;
import com.pmdm.padel.model.Book;

import java.sql.Date;
import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class TodoAdapter extends RecyclerView.Adapter<TodoAdapter.MyViewHolder> {
    private ArrayList<Book> books;

    public TodoAdapter(){
        this.books = new ArrayList<>();
    }

    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ItemViewBinding binding;//Name of the item_view.xml in camel case + "Binding"

        public MyViewHolder(ItemViewBinding b) {
            super(b.getRoot());
            binding = b;
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType){
        return new MyViewHolder(ItemViewBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    // Involves populating data into the item through holder
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        // Get the data model based on position
        Book book = books.get(position);
        String fechaReserva = book.getFecha();
        String horaReserva = book.getHora();

        holder.binding.itemFecha.setText(fechaReserva);
        holder.binding.itemHora.setText(horaReserva);
    }


    @Override
    public int getItemCount() {
        return books.size();
    }

    public int getId(int position){

        return this.books.get(position).getId();
    }

    public void setBooks(ArrayList<Book> books) {
        this.books = books;
        notifyDataSetChanged();
    }

    public Book getAt(int position){
        Book book;
        book = this.books.get(position);
        return book;
    }

    public void add(Book book) {
        this.books.add(book);
        notifyItemInserted(books.size() - 1);
        notifyItemRangeChanged(0, books.size() - 1);
    }

    public void modifyAt(Book book, int position) {
        this.books.set(position, book);
        notifyItemChanged(position);
    }

    public void removeAt(int position) {
        this.books.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(0, books.size() - 1);
    }
}
